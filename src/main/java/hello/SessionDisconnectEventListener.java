package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lulzimgashi on 09/01/2018.
 */
@Component
public class SessionDisconnectEventListener implements ApplicationListener<SessionDisconnectEvent> {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public void onApplicationEvent(SessionDisconnectEvent sessionDisconnectEvent) {
        String useri = GreetingController.users.get(sessionDisconnectEvent.getSessionId());
        GreetingController.users.remove(sessionDisconnectEvent.getSessionId());

        Map<String, String> map = new HashMap<>();
        map.put("disconnect", useri);


        simpMessagingTemplate.convertAndSend("/channel/users", map);
    }
}