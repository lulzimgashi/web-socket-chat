package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

@Controller
public class GreetingController {

    public static Map<String, String> users = new HashMap<>();

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @SubscribeMapping("/channel/subscribe/{id}")
    public Object[] chatInit(SimpMessageHeaderAccessor accessor, @DestinationVariable String id) {

        Object a = accessor.getHeader("stompCommand").toString();
        if(accessor.getHeader("stompCommand").toString().equals("SUBSCRIBE")) {
            String sessionId = (String) accessor.getHeader("simpSessionId");
            users.put(sessionId, id);

            simpMessagingTemplate.convertAndSend("/channel/users", new String[]{id});

            return users.values().toArray();
        }
        return new String[]{};
    }
}
